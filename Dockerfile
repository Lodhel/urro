FROM python:3.9.1-slim-buster

COPY . .
RUN apt update && apt-get install -y gcc
RUN pip install -r requirements.txt

CMD ["/run.sh"]
